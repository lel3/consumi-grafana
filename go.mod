module gitlab.com/eutampieri/consumi-grafana

go 1.15

require (
	github.com/influxdata/influxdb-client-go/v2 v2.10.0 // indirect
	github.com/influxdata/influxdb1-client v0.0.0-20220302092344-a9ab5670611c
)
