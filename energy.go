package main

import "time"

type Rate int

const (
	F1 Rate = iota + 1
	F2
	F3
)

func TimeToRate(t time.Time) Rate {
	if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
		return F3
	} else if t.Hour() == 7 || (t.Hour() >= 19 && t.Hour() <= 22) {
		return F2
	} else {
		return F1
	}
}
