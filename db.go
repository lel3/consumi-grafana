package main

import (
	"fmt"
	influxdb2 "github.com/influxdata/influxdb1-client/v2"
	"os"
	"time"
)

// GetEnergyUsage Connect to an Influx Database reading the credentials from
// environment variables INFLUXDB_TOKEN, INFLUXDB_URL
// return influxdb Client or errors
func GetEnergyUsage(start time.Time, end time.Time) ([]struct {
	time.Time
	float32
}, error) {

	dbURL := os.Getenv("INFLUXDB_URL")
	if dbURL == "" {
		dbURL = "http://[2001:470:cb2b:2::21]:8086"
		//return nil, errors.New("INFLUXDB_URL must be set")
	}

	c, err := influxdb2.NewHTTPClient(influxdb2.HTTPConfig{
		Addr: dbURL,
	})
	if err != nil {
		fmt.Println("Error creating InfluxDB Client: ", err.Error())
	}
	defer c.Close()

	q := influxdb2.NewQuery(
		fmt.Sprintf(
			"SELECT energy FROM %s WHERE device = '%s' AND time >= %s AND time <= %s",
			os.Getenv("INFLUXDB_MEASUREMENT"),
			os.Getenv("DEVICE_ID"),
			start,
			end,
		),
		os.Getenv("INFLUXDB_DB"), "",
	)
	if response, err := c.Query(q); err == nil && response.Error() == nil {
		ts := response.Results[0]
		result := make([]struct {
			time.Time
			float32
		}, 0)
		for _, v := range ts.Series {
			time, err := time.Parse(time.RFC3339Nano, v.Values[0])
			if err == nil {
				continue
			}
			result = append(result, struct {
				time.Time
				float32
			}{time, 3})
		}
		return result, nil
	} else {
		return nil, err
	}
}
